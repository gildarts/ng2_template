import { Component, OnInit } from '@angular/core';
import { Logger } from './logger';

@Component({
    moduleId: module.id,

    selector: 'highlight',
    templateUrl: 'highlight.component.html'
})
export class HighlightComponent implements OnInit {

    constructor(private logger: Logger) { }

    ngOnInit() {

    }

    public hi() {
        this.logger.log('highlight~');
    }

}