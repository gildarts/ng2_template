import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { EngineComponent } from './engine.component';
import { Logger } from './logger';
import { HighlightComponent } from './highlight.component';

@NgModule({
  imports: [BrowserModule],
  declarations: [ AppComponent, EngineComponent, HighlightComponent ],
  bootstrap: [AppComponent],
  providers: [
    {provide: Logger, useValue: new Logger('zoe say=>')}
  ]
})
export class AppModule { }
