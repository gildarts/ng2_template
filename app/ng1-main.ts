import { upgradeAdapter as ua } from './upgrade-adapter';
import './ng1-components';
import './default.less';

ua.bootstrap(document.body, ['ng1'], { strictDi: true });