import { Component, OnInit, ViewChild } from '@angular/core';
import { Logger } from './logger';
import { HighlightComponent } from './highlight.component';

@Component({
    moduleId: module.id,

    selector: 'engine',
    templateUrl: 'engine.component.html',
    styleUrls: [
        'engine.component.css'
    ],
    providers: [
        { provide: Logger, useValue: new Logger('yaoming say=>') }
    ]
})
export class EngineComponent implements OnInit {
    constructor(private logger: Logger) {
        this.items = [
            { title: 'item1' },
            { title: 'item2' },
            { title: 'item3' },
            { title: 'item4' }
        ]
        this.code = Date.now();
    }

    private items: any;

    public code: number;

    @ViewChild('line')
    public line: HighlightComponent;

    public say() {
        this.items.push({
            title: Date.now()
        });
        this.logger.log('engine log...');
        this.line.hi();
    }

    ngOnInit() { }
}