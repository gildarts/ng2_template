import { upgradeAdapter as ua } from './upgrade-adapter';
import { AppComponent } from './app.component';
import { EngineComponent } from './engine.component';

angular.module('ng1')
    .directive("ng2Com", ua.downgradeNg2Component(AppComponent) as ng.IDirectiveFactory);

// angular.module('ng1')
//     .directive("engine", ua.downgradeNg2Component(EngineComponent));

angular.
    module('ng1').
    component('ng1Com', {
        template:`
        <ng2-com>ng2 loading...</ng2-com>
        <ng1-com2>ng1 loading...</ng1-com2>
        <ul>
            <li ng-repeat="phone in $ctrl.phones">
                <span>{{phone.name}}</span>
                <p>{{phone.snippet}}</p>
            </li>
        </ul>
        `,
        controller: function PhoneListController() {
            this.phones = [
                {
                    name: 'Nexus S',
                    snippet: 'Fast just got faster with Nexus S.'
                }, {
                    name: 'Motorola XOOM™ with Wi-Fi',
                    snippet: 'The Next, Next Generation tablet.'
                }, {
                    name: 'MOTOROLA XOOM™',
                    snippet: 'The Next, Next Generation tablet.'
                }
            ];
        }
    });

angular.
    module('ng1').
    component('ng1Com2', {
        template:
        `<h2>this is ng1 component.</h2>`,
        controller: function Ng1Com2() {
        }
    });
