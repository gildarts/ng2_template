import { upgradeAdapter } from './upgrade-adapter';
import { Component, ViewChild, ViewChildren, Inject } from '@angular/core';
import { EngineComponent } from './engine.component';
import * as ng2 from '@angular/core';
import { Logger } from './logger';
import { HighlightComponent } from './highlight.component';

@Component({
  moduleId: module.id,
  selector: 'ng2-com',
  templateUrl: "app.component.html",
  styleUrls: [
    'app.component.css'
  ]
})
export class AppComponent {

  constructor(private logger: Logger) {
    this.name = "Zoe is demon.";

    this.engine_list = [];
    for (let i = 0; i < 10; i++) {
      this.engine_list.push(i);
    }

    this.isPrimary = false;
  }

  public name: string;

  public engine_list: any[];

  public isPrimary: boolean;

  @ViewChild('eng')
  public engine: EngineComponent;

  @ViewChildren('engs')
  public engines: any;

  @ViewChild('h1')
  public h1: ng2.ElementRef;

  @ViewChild('line')
  public line: HighlightComponent;

  private changeCss() {
    this.isPrimary = !this.isPrimary;
  }

  public show(eng: EngineComponent) {
    this.engine.say();
    // let engs: Array<EngineComponent> = this.engines.toArray();
    // engs.push(this.engine);

    // for(let e of engs) {
    //   console.log(e);
    // }

    this.logger.log('show success!!');
    this.line.hi();
  }
}
