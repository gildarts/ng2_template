import { Injectable } from '@angular/core';

export class Logger {
    private name: string;
    
    constructor(name: string) {
        this.name = name;
    }

    public log(msg: string): void {
        console.log(`${this.name}: ${msg}`);
    }
}