/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    transpiler: 'ts_debug',
    typescriptOptions: {
      tsconfig: true, //plugin-typescript option
      typeCheck: false //plugin-typescript option
    },
    'meta': {
      '*.less': { 'loader': 'less_loader' }
    },
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: 'app',
      // angular bundles
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
      '@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
      // other libraries
      'rxjs': 'npm:rxjs',
      'angular2-in-memory-web-api': 'npm:angular2-in-memory-web-api',
      "ts_debug": "npm:plugin-typescript/lib", //宣告 typescript plugin 放在哪裡。
      "typescript": "npm:typescript", //宣告 typescript sdk 放在哪。
      'css': 'npm:systemjs-plugin-css',
      'less_loader': 'npm:systemjs-plugin-less',
      'lesscss': 'npm:less',
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        main: './main',
        defaultExtension: 'ts',
        meta: {
          "*.ts": {
            loader: 'ts_debug'
          }
        }
      },
      rxjs: {
        defaultExtension: 'js'
      },
      'angular2-in-memory-web-api': {
        main: './index.js',
        defaultExtension: 'js'
      },
      "ts_debug": { //宣告 typescript plugin 的進入點。
        "main": "plugin.js"
      },
      "typescript": {
        "main": "lib/typescript.js", //指示 TypeScript 的主程式，TypeScript 目錄要在 map 中指定。
        "meta": {
          "lib/typescript.js": {
            "exports": "ts" // 這行作用是什麼我不確定。
          }
        }
      },
      'less': {
        'defaultExtension': 'less',
      },
      'lesscss': {
        'main': {
          'browser': './dist/less.min.js',
          'node': '@node/less'
        }
      },
      'css': { 'main': 'css.js' },
      'less_loader': { 'main': 'less.js' }
    }
  });
})(this);
